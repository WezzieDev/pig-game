'use strict';

/**
 * GAME RULES:
 * - The game has 2 players, playing in rounds
 * - In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
 * - BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
 * - The player can choose to 'Hold', which means that his ROUND score gets added to his GLOBAL score.
 *   After that, it's the next player's turn.
 * - The first player to reach 100 points on GLOBAL score wins the game
 */

var playerModel = function () {
    this.name = ko.observable('Player Name');
    this.score = ko.observable(0);
    this.roundScore = ko.observable(0);
};

var playerModelFactory = {
    create: function (name) {
        var player = new playerModel();
        player.name(name);

        return player;
    }
};

var pigGameViewModel = {
    players: ko.observableArray(),
    activePlayer: ko.observable(null),
    gameActive: ko.observable(false),
    showDice: ko.observable(false),
    diceNumber: ko.observable(1),
    rolling: ko.observable(false),

    // Reinitialize the game and set all defaults.
    newGame: function () {
        this.cleanup();

        for (var i = 0; i <= 1; i++) {
            var playerName = '';

            while (playerName === null || playerName.length < 1) {
                playerName = prompt("Please enter the name for player " + i + ". To abort starting a new game, type exit.", "");

                if ('exit' === playerName) {
                    this.cleanup();
                    return false;
                }
            }

            this.players.push(playerModelFactory.create(playerName));
        }

        this.gameActive(true);
        this.activePlayer(0);
    },

    cleanup: function () {
        this.players.removeAll();
        this.gameActive(false);
    },

    endGame: function () {
        this.cleanup();
    },

    askEndGame: function () {
        if (confirm("Are you sure you want to end the game?") === true) {
            this.endGame();
        }
    },

    hasPlayer: function (idx) {
        if (this.players.length === 0) {
            return false;
        }

        return this.players.length >= idx;
    },

    diceAttr: function () {
        return {
            src: 'img/dice-' + this.diceNumber() + '.png',
            alt: 'Dice',
            class: ['dice']
        };
    },

    getPlayerRootDivAttr: function (idx) {
        var classes = [];
        classes.push('player-' + idx + '-panel');

        if (this.activePlayer() !== null && idx === this.activePlayer()) {
            classes.push('active');
        }

        return {
            class: classes.join(' ')
        };
    },

    onRollDice: function () {
        var activePlayer = this.players()[this.activePlayer()];

        if (this.diceNumber() === 1) {
            activePlayer.roundScore(0);
            this.switchActivePlayer();
        } else {
            activePlayer.roundScore(activePlayer.roundScore() + this.diceNumber());
        }
    },

    rollDiceRandom: function (viewModel) {
        viewModel.showDice(true);
        viewModel.diceNumber(Math.ceil(Math.random() * 6));
    },

    rollDiceAnimated: function () {
        // Do not roll again if we're already rolling.
        if (this.rolling() === true) {
            return;
        }

        // Start rolling
        this.rolling(true);
        var ticker = setInterval(this.rollDiceRandom, 25, this);
        setTimeout(function (viewModel) {
            clearInterval(ticker);

            viewModel.onRollDice();
            viewModel.rolling(false);
        }, 1000, this);
    },

    hold: function () {
        var activePlayer = this.players()[this.activePlayer()];
        var currentScore = activePlayer.score();
        var roundScore = activePlayer.roundScore();
        var newScore = currentScore + roundScore;

        activePlayer.score(newScore);
        activePlayer.roundScore(0);

        // Before switching, see if we have a winner
        if (newScore >= 100) {
            // We do have a winner!
            alert('Many congratulations to ' + activePlayer.name() + ', you have won the game with ' + newScore + ' points!');
            this.endGame(false);

            return false;
        }

        // Switch player
        this.switchActivePlayer();
    },

    switchActivePlayer: function () {
        if (this.activePlayer() === 0) {
            this.activePlayer(1);
        } else {
            this.activePlayer(0);
        }
    }
};

ko.applyBindings(pigGameViewModel);