'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var autoprefix = require('gulp-autoprefixer');

gulp.task('css:compile', function () {
    return gulp.src('./src/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('app.css'))
        .pipe(autoprefix('last 2 version'))
        .pipe(gulp.dest('./css'));
});

gulp.task('css:watch', function () {
    gulp.watch('./src/scss/**/*.scss', ['css:compile']);
});

gulp.task('default', ['css:compile', 'css:watch']);