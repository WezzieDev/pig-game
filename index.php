<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="css/app.css">

    <title>Pig Game</title>
</head>
<body>
    <div class="wrapper clearfix">
        <div class="players" data-bind="foreach: players">
            <div data-bind="attr: $parent.getPlayerRootDivAttr($index())">
                <div class="player-name" data-bind="text: name"></div>
                <div class="player-score" data-bind="text: score"></div>
                <div class="player-current-box">
                    <div class="player-current-label">Current</div>
                    <div class="player-current-score" data-bind="text: roundScore"></div>
                </div>
            </div>
        </div>

        <button class="btn-new" data-bind="click: newGame, visible: !gameActive()"><i class="ion-ios-plus-outline"></i>New game</button>
        <button class="btn-new" data-bind="click: askEndGame, visible: gameActive()"><i class="ion-ios-minus-outline"></i>End game</button>
        <button class="btn-roll" data-bind="click: rollDiceAnimated, visible: gameActive()"><i class="ion-ios-loop"></i>Roll dice</button>
        <button class="btn-hold" data-bind="click: hold, visible: gameActive()"><i class="ion-ios-download-outline"></i>Hold</button>

        <img data-bind="visible: showDice, attr: diceAttr()"/>
    </div>

    <script type="text/javascript" src="./node_modules/knockout/build/output/knockout-latest.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
</body>
</html>